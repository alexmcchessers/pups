using Godot;
using System;

namespace com.washingup.pups.enemies
{
    public class RedFighter : Enemy
    {
        private Vector3 velocity;

        private Spatial[] destinationNodes = null;

        int currentNodeIndex = 0;

        private Spatial destinationNode;

        private static readonly float SPEED = 150f;

        public void SetDestinationNodes(Spatial[] nodes)
        {
            destinationNodes = nodes;
            currentNodeIndex = 0;
            destinationNode = nodes[0];
        }

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            
        }

        protected override int getScore()
        {
            return 150;
        }

        public override void _PhysicsProcess(float delta)
        {
            if(destinationNode!=null)
            {
                velocity = (destinationNode.Translation - Translation).Normalized() * SPEED;
                LookAt(destinationNode.Translation, Vector3.Up);
            }

            MoveAndCollide(velocity * delta);
        }
    }
}
