using com.washingup.pups.shots;
using Godot;
using System;

namespace com.washingup.pups.enemies
{
    public class AlienHead : Enemy
    {
        public Vector3 Velocity;

        private Vector3 oldVelocity;

        private static readonly Vector3 VERTICAL = new Vector3(0, -15, 0);

        private float verticalDest;

        private float timeSinceLastShot = 0f;

        public double minShotTime = 1f;

        public double maxShotTime = 60f;

        private double nextShotTime = 0f;

        private static PackedScene shotScene = (PackedScene)ResourceLoader.Load("res://Scenes/Entities/Shots/BasicEnemyShot.tscn");

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            setNextShotTime();
        }

        private void setNextShotTime()
        {
            nextShotTime = new Random().NextDouble() * (maxShotTime-minShotTime) + minShotTime;
        }

        public override void _PhysicsProcess(float delta)
        {
            MoveAndCollide(Velocity * delta);

            if(Velocity.y != 0 && Translation.y <= verticalDest)
            {
                Translation = new Vector3(Translation.x, verticalDest, Translation.z);
                Velocity = new Vector3(oldVelocity.x * -1, 0, 0);
            }
        }

        public void Descend()
        {
            oldVelocity = Velocity;
            Velocity = VERTICAL;
            verticalDest = Translation.y - 5;
        }

        public override void _Process(float delta)
        {
            timeSinceLastShot+=delta;
            if(timeSinceLastShot>=nextShotTime)
            {
                fire();
                timeSinceLastShot = 0f;

            }
        }

        private void fire()
        {
            BasicEnemyShot newShot = (BasicEnemyShot)shotScene.Instance();
            newShot.SetTranslation(new Vector3(Translation.x, Translation.y, Translation.z-0.1f));
            GetTree().GetRoot().AddChild(newShot);
        }

        protected override int getScore()
        {
            return 100;
        }

    }

}
