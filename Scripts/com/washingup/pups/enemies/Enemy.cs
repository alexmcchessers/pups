using Godot;
using System;

using com.washingup.pups.spawners;
using com.washingup.pups.drops;

namespace com.washingup.pups.enemies
{
    public abstract class Enemy : KinematicBody
    {
        private Spawner spawner;

        private static PackedScene dropScene = (PackedScene)ResourceLoader.Load("res://Scenes/Entities/Drop.tscn");

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            
        }

        public void SetSpawner(Spawner spawner)
        {
            this.spawner = spawner;
        }

        public void OnDestroyed()
        {
            if(spawner!=null)
            {
                spawner.OnEnemyDeath();
            }

            SpawnDrop();
            GameController.Instance.AddToScore(getScore());
        }

        // Call when the enemy is to be removed but not destroyed by the player, for example, when going off screen.
        public void NonDestroyedRemove()
        {
            if(spawner!=null)
            {
                spawner.OnEnemyDeath();
            }
            QueueFree();
        }

        private void SpawnDrop()
        {
            // Create the drop object
            Drop drop = (Drop)dropScene.Instance();
            drop.SetTranslation(new Vector3(Translation.x, Translation.y, Translation.z-0.1f));
            drop.OnSpawn();
            GetTree().GetRoot().AddChild(drop);
        }

        protected abstract int getScore();

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }
    }
}
