using Godot;
using System;

namespace com.washingup.pups
{
    public class PlayerController : KinematicBody
    {
        [Export]
        private int maxSpeed = 250;

        [Export]
        private int friction = 10;

        [Export]
        private int acceleration = 50;

        [Export]
        private float fireDelay = 0.1f;

        [Export]
        public bool verticalEnabled = false;

        private float timeSinceLastFired;

        private Vector3 velocity = new Vector3();

        private PackedScene shotScene = (PackedScene)ResourceLoader.Load("res://Scenes/Entities/PlayerShot.tscn");

        private static PlayerController instance;

        public static PlayerController Instance {
            get 
            {
                return instance;
            }
        }


        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            instance = this;
            timeSinceLastFired = fireDelay;
        }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }

        public override void _PhysicsProcess(float delta)
        {
            HandleMovement(delta);
            HandleFire(delta);
            MoveAndCollide(velocity * delta);
        }

        private void HandleMovement(float delta)
        {
            if (Input.IsActionPressed("right"))
            {
                if(velocity.x < 0)
                {
                    velocity.x = 0;
                }
                velocity.x += acceleration * delta;
                if(velocity.x > maxSpeed)
                {
                    velocity.x = maxSpeed;
                }
            }
            else if (Input.IsActionPressed("left"))
            {
                if(velocity.x > 0)
                {
                    velocity.x = 0;
                }
                velocity.x -= acceleration * delta;
                if(Math.Abs(velocity.x) > maxSpeed)
                {
                    velocity.x = -maxSpeed;
                }
            }
            else
            {
                // Apply friction
                if(velocity.x < 0)
                {
                    velocity.x += friction * delta;
                    if(velocity.x > 0)
                    {
                        velocity.x = 0;
                    }
                } 
                else if(velocity.x > 0) 
                {
                    velocity.x -= friction * delta;
                    if(velocity.x < 0)
                    {
                        velocity.x = 0;
                    }
                }
            }


            if (verticalEnabled && Input.IsActionPressed("up"))
            {
                if(velocity.y < 0)
                {
                    velocity.y = 0;
                }

                velocity.y += acceleration * delta;
                if(velocity.y > maxSpeed)
                {
                    velocity.y = maxSpeed;
                }
            }
            else if (verticalEnabled && Input.IsActionPressed("down"))
            {
                if(velocity.y > 0)
                {
                    velocity.y = 0;
                }

                velocity.y -= acceleration * delta;
                if(Math.Abs(velocity.y) > maxSpeed)
                {
                    velocity.y = -maxSpeed;
                }
            }    
            else
            {
                // Apply friction
                if(velocity.y < 0)
                {
                    velocity.y += friction * delta;
                    if(velocity.y > 0)
                    {
                        velocity.y = 0;
                    }
                } 
                else if(velocity.y > 0) 
                {
                    velocity.y -= friction * delta;
                    if(velocity.y < 0)
                    {
                        velocity.y = 0;
                    }
                }
            }       

            if(Translation.x < -55)
            {
                Translation = new Vector3(-55, Translation.y, Translation.z);
            }
            else if(Translation.x > 55)
            {
                Translation = new Vector3(55, Translation.y, Translation.z);
            }

            if(Translation.y < -30)
            {
                Translation = new Vector3(Translation.x, -30, Translation.z);
            }
            else if(Translation.y > 30)
            {
                Translation = new Vector3(Translation.x, 30, Translation.z);
            }
        }

        private void HandleFire(float delta)
        {
            timeSinceLastFired += delta;

            if(timeSinceLastFired > fireDelay && Input.IsActionPressed("fire"))
            {
                timeSinceLastFired = 0;
                
                PlayerShot newShot = (PlayerShot)shotScene.Instance();
                newShot.SetTranslation(GetTranslation());
                GetTree().GetRoot().AddChild(newShot);
            }
        }
    }

}
