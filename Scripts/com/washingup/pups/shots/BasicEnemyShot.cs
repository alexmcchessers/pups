using Godot;
using System;

namespace com.washingup.pups.shots
{
    public class BasicEnemyShot : KinematicBody
    {
       private Vector3 velocity = new Vector3(0, -25, 0);

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {

        }

        public override void _PhysicsProcess(float delta)
        {
            KinematicCollision collision = MoveAndCollide(velocity * delta);
            if(collision!=null)
            {
                Health healthNode = (Health)((Node)collision.GetCollider()).FindNode("Health");

                if(healthNode!=null)
                {
                    healthNode.TakeDamage(1);
                }
                
                QueueFree();
            }
        }

        public void _on_VisibilityNotifier_screen_exited()
        {
            QueueFree();
        }
    }
}
