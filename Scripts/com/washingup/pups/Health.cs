using Godot;
using System;

namespace com.washingup.pups
{
    public class Health : Node
    {
        [Export]
        private int health;

        [Signal]
        public delegate void Destroyed();

        private static PackedScene explosionScene = (PackedScene)ResourceLoader.Load("res://Scenes/Entities/Explosions/SmallExplosion.tscn");

        private KinematicBody parent;

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            parent = (KinematicBody)GetParent();
        }

        public void TakeDamage(int damage)
        {
            health -= damage;

            if(health<=0)
            {
                Explosion explosion = (Explosion)explosionScene.Instance();
                explosion.SetTranslation(parent.GetTranslation());
                GetTree().GetRoot().AddChild(explosion);
                explosion.Explode(true);
                
                EmitSignal(nameof(Destroyed));

                parent.QueueFree();
            }
        }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }
    }
}