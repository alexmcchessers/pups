using Godot;
using System;

namespace com.washingup.pups
{
    public class Explosion : Particles
    {
        private bool startedOnce = false;

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {

        }

        //  // Called every frame. 'delta' is the elapsed time since the previous frame.
        public override void _Process(float delta)
        {
            if(startedOnce && !IsEmitting())
            {
                QueueFree(); 
            }    
        }

        public void Explode(Boolean playSound)
        {
            startedOnce = true;
            base.SetEmitting(true);
            if(playSound)
            {
                AudioStreamPlayer sound = (AudioStreamPlayer)GetNode("ExplosionSound");
                sound.Play();
            }
        }
    }
}
