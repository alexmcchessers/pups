using Godot;
using System;
using System.Collections.Generic;
using System.Text;

namespace com.washingup.pups.ui
{
    public class Console : Label
    {
        private List<string> messageQueue = new List<string>();

        private List<string> visibleMessages = new List<string>();

        private static readonly int TOTAL_MESSAGES = 10;

        private static readonly float CHARACTER_DELAY = 0.01f;

        private float timeSinceLastCharacter = 0f;

        private int charIndex = 0;

        private string currentString;

        private static Console instance;

        public static Console Instance {
            get 
            {
                return instance;
            }
        }

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            instance = this;
        }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
        public override void _Process(float delta)
        {
            if(messageQueue.Count>0)
            {
                timeSinceLastCharacter += delta;
                if(timeSinceLastCharacter >= CHARACTER_DELAY)
                {
                    if(charIndex == 0)
                    {
                        visibleMessages.Add("");

                        if(visibleMessages.Count > TOTAL_MESSAGES)
                        {
                            visibleMessages.RemoveAt(0);
                        }
                    }

                    visibleMessages[visibleMessages.Count-1] += messageQueue[0][charIndex];

                    charIndex++;
                    if(charIndex == messageQueue[0].Length)
                    {
                        messageQueue.RemoveAt(0);
                        charIndex = 0;
                    }

                    StringBuilder stringBuilder = new StringBuilder();
                    for(int i = 0; i < visibleMessages.Count; i++)
                    {
                        stringBuilder.Append(visibleMessages[i]);

                        if(i+1!=visibleMessages.Count)
                        {
                            stringBuilder.Append('\n');
                        }
                        else
                        {
                            stringBuilder.Append('_');
                        }
                    }

                    SetText(stringBuilder.ToString());
                    

                    timeSinceLastCharacter = 0f;
                }
            }
        }

        public void QueueMessage(string msg)
        {
            messageQueue.Add(msg);
        }
    }
}
