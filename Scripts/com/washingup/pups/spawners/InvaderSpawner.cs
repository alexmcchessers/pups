using com.washingup.pups.enemies;
using Godot;
using System;

namespace com.washingup.pups.spawners
{
    public class InvaderSpawner : Spawner
    {
        private Vector3 spawnPoint = new Vector3(-12.5f, 25, -50);

        private static readonly float SPAWN_DELAY = 0.05f;
        
        private float  timeSinceLastSpawn = 0f;

        private int leftToSpawn = 18;

        private PackedScene enemyScene = (PackedScene)ResourceLoader.Load("res://Scenes/Entities/Enemies/AlienHead.tscn");

        public override int GetLeftToSpawn()
        {
            return leftToSpawn;
        }

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            
        }

        //  // Called every frame. 'delta' is the elapsed time since the previous frame.
        public override void _Process(float delta)
        {
            if(leftToSpawn > 0)
            {
                timeSinceLastSpawn += delta;

                if(timeSinceLastSpawn>=SPAWN_DELAY)
                {
                    SpawnEnemy();
                    leftToSpawn--;
                    liveEnemies++;
                    timeSinceLastSpawn = 0;

                    if(leftToSpawn==0)
                    {
                        Godot.Collections.Array toStart = GetChildren();
                        foreach(AlienHead child in toStart)
                        {
                            child.Velocity = new Vector3(10, 0, 0);
                        }
                    }
                }
            }

            Godot.Collections.Array children = GetChildren();
            foreach(AlienHead child in children)
            {
                if((child.Velocity.x > 0 && child.GetTranslation().x > 55) || (child.Velocity.x < 0 && child.GetTranslation().x < -55))
                {
                    Descend();
                    break;
                }
            }
        }

        private void Descend()
        {
            Godot.Collections.Array children = GetChildren();
            foreach(AlienHead child in children)
            {
                child.Descend();
            }
        }

        private void SpawnEnemy()
        {
            AlienHead enemy = (AlienHead)enemyScene.Instance();
            enemy.SetTranslation(spawnPoint);
            enemy.SetSpawner(this);
            AddChild(enemy);

            float newX = spawnPoint.x + 5;
            float newY = spawnPoint.y;
            if(newX > 12.5f)
            {
                newX = -12.5f;
                newY -= 5;
            }
            spawnPoint = new Vector3(newX, newY, spawnPoint.z);
        }
    }
}