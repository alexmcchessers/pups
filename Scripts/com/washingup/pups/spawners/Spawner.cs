using Godot;
using System;

namespace com.washingup.pups.spawners
{
    public abstract class Spawner : Spatial
    {
        protected int liveEnemies = 0;

        protected int level = 0;

        public int LiveEnemies 
        { 
            get{ return liveEnemies; }
        }

        public void OnEnemyDeath()
        {
            GD.Print("On enemy death");
            liveEnemies--;
            GD.Print("Remaining: " + liveEnemies);
        }

        public abstract int GetLeftToSpawn();

        public void SetLevel(int level)
        {
            this.level = level;
        }
    }
}
