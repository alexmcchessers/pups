using com.washingup.pups.spawners;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

namespace com.washingup.pups
{
    public class GameController : Spatial
    {
        private int level = 0;

        private int score = 0;

        private int lives = 3;

        private string[][] spawnerScenes = {
            new string[] {
                "res://Scenes/Spawners/InvaderSpawner.tscn"
            }
        };

        private static readonly int TOTAL_LEVELS = 1;

        private int wavesThisLevel = 0;

        private Spawner naturalSpawner = null;

        private List<Spawner> additionalSpawners = new List<Spawner>();

        private Label scoreLabel;

        private Label livesLabel;

        private static GameController instance;

        public static GameController Instance {
            get 
            {
                return instance;
            }
        }

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            instance = this;

            livesLabel = (Label)GetNode("StatusGUI/LivesLabel");
            scoreLabel = (Label)GetNode("StatusGUI/ScoreLabel");

        }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
      public override void _Process(float delta)
      {
        if(naturalSpawner == null || (naturalSpawner.LiveEnemies == 0 && naturalSpawner.GetLeftToSpawn() == 0))      
        {
            GD.Print("New natural spawner");
            wavesThisLevel++;

            if(wavesThisLevel==4)
            {
                GD.Print("Level up");
                // Time to go up a level
                AdvanceLevel();
            }

            if(naturalSpawner!=null)
            {
                GD.Print("Level up");
                naturalSpawner.QueueFree();
            }

            naturalSpawner = CreateNewSpawner();
        }

        List<Spawner> toRemove = null;
        foreach(Spawner spawner in additionalSpawners)
        {
            if(spawner.LiveEnemies == 0 && spawner.GetLeftToSpawn() == 0)
            {
                spawner.QueueFree();

                if(toRemove==null)
                {
                    toRemove = new List<Spawner>();
                    toRemove.Add(spawner);
                }
            }
        }

        if(toRemove!=null)
        {
            additionalSpawners = additionalSpawners.Except(toRemove).ToList();
        }
      }

      private Spawner CreateNewSpawner()
      {
            string[] levelSpawnerScenes = spawnerScenes[level];
            string spawnerSceneName = levelSpawnerScenes[new Random().Next(levelSpawnerScenes.Length)];
            
            PackedScene spawnerScene = (PackedScene)ResourceLoader.Load(spawnerSceneName);

            Spawner spawner = (Spawner)spawnerScene.Instance();
            spawner.SetTranslation(new Vector3(0,0,0));
            spawner.SetLevel(level);
            AddChild(spawner);
            return spawner;
      }

      public void AddExtraSpawner()
      {
          Spawner spawner = CreateNewSpawner();
          additionalSpawners.Add(spawner);
      }

      public void AdvanceLevel()
      {
        level++;
        if(level==TOTAL_LEVELS)
        {
            level = 0;
        }
        wavesThisLevel = 0;
      }

      public void AddToScore(int toAdd)
      {
          score+=toAdd;

            GD.Print("Add to score");
            if(scoreLabel==null)
            {
                GD.Print("No score label!!");
            }

          scoreLabel.Text = score.ToString();
      }
    }
}