using Godot;
using System;

namespace com.washingup.pups
{
    public class ParticlesCache : CanvasLayer
    {
        private static PackedScene explosionScene = (PackedScene)ResourceLoader.Load("res://Scenes/Entities/Explosions/SmallExplosion.tscn");

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            CallDeferred("CacheExplosion");
        }

        private void CacheExplosion()
        {
            Explosion explosion = (Explosion)explosionScene.Instance();
            explosion.Explode(false);
            explosion.SetTranslation(new Vector3(0,0,-90));
            AddChild(explosion);
        }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }
    }
}
