using Godot;
using System;

namespace com.washingup.pups.drops
{
    public class SpawnWave : DropEffect
    {
        // Declare member variables here. Examples:
        // private int a = 2;
        // private string b = "text";

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            
        }

        public override bool IsValidEffect()
        {
            return true;
        }

        public override string TriggerEffect()
        {
            GameController game = (GameController)GetNode("/root/Game");
            game.AddExtraSpawner();
            return "WAVE SPAWN";
        }
    }
}
