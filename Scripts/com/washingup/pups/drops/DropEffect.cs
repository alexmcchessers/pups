using Godot;
using System;

namespace com.washingup.pups.drops
{
    public abstract class DropEffect : Node
    {
        public abstract bool IsValidEffect();
        public abstract string TriggerEffect();
    }
}
