using Godot;
using System;

namespace com.washingup.pups.drops
{

    public class Drop : KinematicBody
    {
        private Vector3 velocity = new Vector3(0, -20, 0);

        private static readonly int TOTAL_DROP_EFFECTS = 3;

        private DropEffect effect = null;

        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            
        }

        public void OnSpawn()
        {
            bool dropValid = false;
            int dropNumber;
            do
            {
                dropNumber = new Random().Next(1, TOTAL_DROP_EFFECTS+1);
                PackedScene effectScene = (PackedScene)ResourceLoader.Load("res://Scenes/Entities/DropEffects/Drop" + dropNumber + ".tscn");
                DropEffect newEffect = (DropEffect)effectScene.Instance();

                dropValid = newEffect.IsValidEffect();
                // If not true, then this drop effect is not valid at the moment and we have to pick another one.
                if(!dropValid)
                {
                    newEffect.QueueFree();
                }
                else
                {
                    AddChild(newEffect);
                    effect = newEffect;
                }
            }
            while(!dropValid);

            Sprite3D sprite = (Sprite3D)FindNode("Sprite3D");
            Texture tex = (Texture)ResourceLoader.Load("res://Sprites/drops/" + dropNumber + ".png");
            sprite.SetTexture(tex);
        }

        public override void _PhysicsProcess(float delta)
        {
            KinematicCollision collision = MoveAndCollide(velocity * delta);
            if(collision!=null)
            {
                com.washingup.pups.ui.Console.Instance.QueueMessage(effect.TriggerEffect());
                QueueFree();
            }
        }
    }
}
