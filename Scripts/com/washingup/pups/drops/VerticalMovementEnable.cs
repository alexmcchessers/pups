using Godot;
using System;

namespace com.washingup.pups.drops
{
    public class VerticalMovementEnable : DropEffect
    {
        public override bool IsValidEffect()
        {
            return !PlayerController.Instance.verticalEnabled;
        }

        public override string TriggerEffect()
        {
            PlayerController.Instance.verticalEnabled = true;
            return "VERTICAL MOVEMENT ENABLED";
        }
        // Called when the node enters the scene tree for the first time.
        public override void _Ready()
        {
            
        }

    //  // Called every frame. 'delta' is the elapsed time since the previous frame.
    //  public override void _Process(float delta)
    //  {
    //      
    //  }
    }
}
